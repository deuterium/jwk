package cloud.deuterium.security.jwt;

import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.text.ParseException;
import java.util.Base64;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void testEncrypter() throws Exception {
		String SECRET_KEY = "0dGHz0sfFSYfzFAt65LNjwjpiLDHyyhf";
		System.out.println(SECRET_KEY.getBytes().length);
		DirectEncrypter encrypter = new DirectEncrypter(SECRET_KEY.getBytes());
		Assert.assertNotNull(encrypter);

	}

	@Test
	public void testVerifier() throws Exception{

		String signedToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJlIjoiQVFBQiIsImtpZCI6ImMwOTZlZTE5LWU1ZTktNDFkMy1hYWRmLTc1MmI0MDJjN2FmOCIsIm4iOiJ2bnA5Z1MyUlMyOU91dVJqbjNaYWhiY2ZKLVFTdnFvXzQ0bTgwakZBV1FqOXo0a2pBZVBJY3J4cTdfTkNsUklYV1hDNVVKb1l5bWZyLTFiNEpBY3JhelRjaWpYV0QtaUFWMlBXdGV1bTg0c0I0bl9yZTRlOHpseWpJRUhPeW4tcVAtNlpBX3ZqYWlTUko1UVhtcTFVb056RDByZ3I1d1padkE4bWNYX2Q5NUxWQ1NSem1pb3lYV1NkUkxkWEtkaVMzaWhtRVVFUFcwS3piZWd0RlpBSWMtd1N4S3p5NHR3T1VtaVFlTDhiWnVqVWc5UUc2alRYZUVXb2ZjY2VtUEpwWU1oWnl4dkxpdFhsRGNtU01MRFIxU2RZWVdjd211eTdrbGRWSENXRzRjTlBvN3dxUGNmdkFVQmVxSjludDY4QnlsQnk5Ml9Cc2o1Vk53LWJZX3J6aFEifX0.eyJzdWIiOiJNaWxhbiIsImlzcyI6Imh0dHA6XC9cL2RldXRlcml1bS5jbG91ZCIsImV4cCI6MTU3MDcxMjUzOCwiaWF0IjoxNTcwNzEyMjM4LCJhY2NvdW50IjpmYWxzZSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9BRE1JTiJdLCJqdGkiOiI4YjYwMDRhNC03YjFlLTRhYzItYThlMS0zNDdmNmQxNGNkNDEifQ.ZvLqzp_ZOs6d7JaWmhYqWr6bv5Nzn6m8K5P1-GUdHKrPS6d0OyhlUWejZcnARFji9NZarAI3VTtfqW_fmcmLHDh0nb8vZJVpD8nOCt0NI_4AKh74oukscMJ0VvTMFvBjSD4CppCcypD9OBxgh3HQwrv2iUbwEGTBEYAEocAeqDcGnUUGY-QMyhvpWaoyvnw9o6UegvjQSKRghq_7ulNDkhlz5C1ygYE85Kyq2406B5918GczyYv-PzNIiarNky3Fabql_KKfDmCvGm1s9QyqftlXR188PtLFOTmCd7hVOgMy3veb4_s2zDsiO1EnC4p6dNIOk-EbBzQN-xIq6qqBUQ";

		SignedJWT signedJWT = SignedJWT.parse(signedToken);
		RSAKey publicKey = RSAKey.parse(signedJWT.getHeader().getJWK().toJSONObject());
		String keyID = signedJWT.getHeader().getKeyID();

		signedJWT.getHeader().getJWK().toPublicJWK();

//		System.out.println("Public key: " + publicKey.getKeyUse().getValue());

		boolean verify = signedJWT.verify(new RSASSAVerifier(publicKey));

		Assert.assertTrue(verify);
	}

	@Test
	public void testClaims() throws ParseException {
		String signedToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp3ayI6eyJrdHkiOiJSU0EiLCJlIjoiQVFBQiIsImtpZCI6ImMwOTZlZTE5LWU1ZTktNDFkMy1hYWRmLTc1MmI0MDJjN2FmOCIsIm4iOiJ2bnA5Z1MyUlMyOU91dVJqbjNaYWhiY2ZKLVFTdnFvXzQ0bTgwakZBV1FqOXo0a2pBZVBJY3J4cTdfTkNsUklYV1hDNVVKb1l5bWZyLTFiNEpBY3JhelRjaWpYV0QtaUFWMlBXdGV1bTg0c0I0bl9yZTRlOHpseWpJRUhPeW4tcVAtNlpBX3ZqYWlTUko1UVhtcTFVb056RDByZ3I1d1padkE4bWNYX2Q5NUxWQ1NSem1pb3lYV1NkUkxkWEtkaVMzaWhtRVVFUFcwS3piZWd0RlpBSWMtd1N4S3p5NHR3T1VtaVFlTDhiWnVqVWc5UUc2alRYZUVXb2ZjY2VtUEpwWU1oWnl4dkxpdFhsRGNtU01MRFIxU2RZWVdjd211eTdrbGRWSENXRzRjTlBvN3dxUGNmdkFVQmVxSjludDY4QnlsQnk5Ml9Cc2o1Vk53LWJZX3J6aFEifX0.eyJzdWIiOiJNaWxhbiIsImlzcyI6Imh0dHA6XC9cL2RldXRlcml1bS5jbG91ZCIsImV4cCI6MTU3MDcxMjUzOCwiaWF0IjoxNTcwNzEyMjM4LCJhY2NvdW50IjpmYWxzZSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9BRE1JTiJdLCJqdGkiOiI4YjYwMDRhNC03YjFlLTRhYzItYThlMS0zNDdmNmQxNGNkNDEifQ.ZvLqzp_ZOs6d7JaWmhYqWr6bv5Nzn6m8K5P1-GUdHKrPS6d0OyhlUWejZcnARFji9NZarAI3VTtfqW_fmcmLHDh0nb8vZJVpD8nOCt0NI_4AKh74oukscMJ0VvTMFvBjSD4CppCcypD9OBxgh3HQwrv2iUbwEGTBEYAEocAeqDcGnUUGY-QMyhvpWaoyvnw9o6UegvjQSKRghq_7ulNDkhlz5C1ygYE85Kyq2406B5918GczyYv-PzNIiarNky3Fabql_KKfDmCvGm1s9QyqftlXR188PtLFOTmCd7hVOgMy3veb4_s2zDsiO1EnC4p6dNIOk-EbBzQN-xIq6qqBUQ";
		SignedJWT signedJWT = SignedJWT.parse(signedToken);
		JWTClaimsSet claims = signedJWT.getJWTClaimsSet();
		Assert.assertNotNull(claims);
	}

	@Test
	public void testPublicKey() throws Exception{

		System.out.println("Generating key pair...");
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(2048);
		KeyPair keyPair = generator.generateKeyPair();

		PublicKey aPublic = keyPair.getPublic();

		Assert.assertNotNull(aPublic);


	}

}

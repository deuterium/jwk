package cloud.deuterium.security.jwt.service;

import com.nimbusds.jose.JOSEException;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

/**
 * Created by MilanNuke on 10/10/2019
 */

public interface TokenService {
    String createJwtDecryptedToken() throws JOSEException, NoSuchAlgorithmException;
    String decryptToken(String encryptedToken) throws JOSEException, ParseException;
    boolean validateJwtTokenSignature(String signedToken) throws AccessDeniedException, JOSEException, ParseException;
    JSONObject createJwkSet();
    JSONObject getFromKeycloak() throws IOException, ParseException;
}

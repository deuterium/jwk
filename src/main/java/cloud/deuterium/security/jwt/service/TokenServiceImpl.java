package cloud.deuterium.security.jwt.service;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.nio.file.AccessDeniedException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by MilanNuke on 10/10/2019
 */

@Service
public class TokenServiceImpl implements TokenService{

    private Logger logger = LoggerFactory.getLogger(this.getClass());
//    private final String SECRET_KEY = "0dGHz0sfFSYfzFAt65LNjwjpiLDHyyhf2BE78eBnztStuOBpOd6yJ7x2tVhyvxMP";
    private final String SECRET_KEY = "0dGHz0sfFSYfzFAt65LNjwjpiLDHyyhf";


    @Override
    public String createJwtDecryptedToken() throws JOSEException, NoSuchAlgorithmException {

        logger.info("Create JWT Token");

        SignedJWT signedJWT = createSignedJWT();
        String encryptToken = encryptToken(signedJWT);

        logger.info("JWT Token successfully created: {}", encryptToken);
        return encryptToken;
    }

    private SignedJWT createSignedJWT() throws NoSuchAlgorithmException, JOSEException {
        logger.info("Generating signed JWT...");
        JWTClaimsSet jwtClaimsSet = createJWTClaimsSet();
        KeyPair keyPair = createKeyPair();

        JWK jwk = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
                .keyID(UUID.randomUUID().toString())
                .build();

        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .jwk(jwk)
                .type(JOSEObjectType.JWT)
                .build();

        SignedJWT signedJWT = new SignedJWT(header, jwtClaimsSet);

        RSASSASigner signer = new RSASSASigner(keyPair.getPrivate());

        logger.info("Signing JWT... ");
        logger.info("Public key: {}", Base64.encode(keyPair.getPublic().getEncoded()));
        logger.info("Public key: {}", new String(keyPair.getPublic().getEncoded()));
        logger.info("Public key: {}", java.util.Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
        signedJWT.sign(signer);

        logger.info("JWT was signed: {}", signedJWT.serialize());

        return signedJWT;
    }

    private JWTClaimsSet createJWTClaimsSet(){
        logger.info("Generating claims...");
        return new JWTClaimsSet.Builder()
                .subject("Milan")
                .issuer("http://deuterium.cloud")
                .claim("account", false)
                .claim("authorities", Stream
                                                .of("ROLE_USER", "ROLE_ADMIN")
                                                .collect(Collectors.toList()))
                .issueTime(new Date())
                .expirationTime(new Date(System.currentTimeMillis() + 5 * 60 * 1000))
                .jwtID(UUID.randomUUID().toString())
                .build();
    }

    private KeyPair createKeyPair() throws NoSuchAlgorithmException {
        logger.info("Generating key pair...");
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048);
        return generator.generateKeyPair();
    }

    private String encryptToken(SignedJWT signedJWT) throws JOSEException {
        logger.info("Encrypting token...");

        DirectEncrypter encrypter = new DirectEncrypter(SECRET_KEY.getBytes());

        JWEObject jweObject = new JWEObject(new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A128CBC_HS256)
                .contentType("JWT")
                .build(), new Payload(signedJWT));

        logger.info("Encrypting token with system's secret key");

        jweObject.encrypt(encrypter);

        logger.info("Token encrypted: {}", jweObject.serialize());

        return jweObject.serialize();
    }

    @Override
    public String decryptToken(String encryptedToken) throws JOSEException, ParseException {
        logger.info("Decrypting token...");

        JWEObject jweObject = JWEObject.parse(encryptedToken);

        DirectDecrypter directDecrypter = new DirectDecrypter(SECRET_KEY.getBytes());

        jweObject.decrypt(directDecrypter);

        logger.info("Token decrypted, returning signed token . . . ");

        return jweObject.getPayload().toSignedJWT().serialize();
    }

    @Override
    public boolean validateJwtTokenSignature(String signedToken) throws AccessDeniedException, JOSEException, ParseException {
        logger.info("Starting method to validate token signature...");

        SignedJWT signedJWT = SignedJWT.parse(signedToken);

        logger.info("Token Parsed! Retrieving public key from signed token");

        RSAKey publicKey = RSAKey.parse(signedJWT.getHeader().getJWK().toJSONObject());

        logger.info("Public key retrieved, validating signature. . . ");

        if (!signedJWT.verify(new RSASSAVerifier(publicKey)))
            throw new AccessDeniedException("Invalid token signature!");

        logger.info("The token has a valid signature");

        return true;
    }


    @Override
    public JSONObject createJwkSet(){

//        RSAKey.Builder builder = new RSAKey.Builder((RSAPublicKey) keyPair().getPublic())
//                .keyUse(KeyUse.SIGNATURE)
//                .algorithm(JWSAlgorithm.RS256)
//                .keyID("bael-key-id");
//        JWKSet jwkSet = new JWKSet();
//        return new JWKSet(builder.build());

        List<JWK> jwks = new ArrayList<>();

        // First key
        String xApiKey = UUID.randomUUID().toString();
        JWK jwk = make(2048, KeyUse.SIGNATURE, new Algorithm("PS512"), xApiKey);

        jwks.add(jwk);

        // Second key
        xApiKey = UUID.randomUUID().toString();
        jwk = make(2048, KeyUse.SIGNATURE, new Algorithm("PS512"), xApiKey);

        jwks.add(jwk);

        // Third key
        xApiKey = UUID.randomUUID().toString();
        jwk = make(2048, KeyUse.SIGNATURE, new Algorithm("PS512"), xApiKey);


        jwks.add(jwk);

        JWKSet jwkSet = new JWKSet(jwks);


//        return jwkSet.toPublicJWKSet().toJSONObject();
        return jwkSet.toJSONObject();

    }

    private static RSAKey make(Integer keySize, KeyUse keyUse, Algorithm keyAlg, String kid) {

        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(keySize);
            KeyPair kp = generator.generateKeyPair();

            RSAPublicKey pub = (RSAPublicKey) kp.getPublic();
            System.out.println(java.util.Base64.getEncoder().encodeToString(pub.getEncoded()));
            RSAPrivateKey priv = (RSAPrivateKey) kp.getPrivate();

            return new RSAKey.Builder(pub)
                    .privateKey(priv)
                    .keyUse(keyUse)
                    .algorithm(keyAlg)
                    .keyID(kid)
                    .build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public JSONObject getFromKeycloak() throws IOException, ParseException {
        String PUBLIC_KEY_VERIFICATION_URL = "http://devrfz:8080/auth/realms/Badin/protocol/openid-connect/certs";

        JWKSet jwkSet = JWKSet.load(new URL(PUBLIC_KEY_VERIFICATION_URL));

        return jwkSet.toJSONObject();
    }

    private PublicKey getPublicKey(String pk) {
        try {
            byte[] byteKey = java.util.Base64.getDecoder().decode(pk);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            logger.info("Public KEY successfully initialized!");
            return kf.generatePublic(X509publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.error("Public KEY is NOT initialized!");
        return null;
    }

}

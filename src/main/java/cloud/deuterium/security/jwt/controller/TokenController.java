package cloud.deuterium.security.jwt.controller;

import cloud.deuterium.security.jwt.service.TokenService;
import com.nimbusds.jose.JOSEException;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

/**
 * Created by MilanNuke on 10/10/2019
 */

@RestController
@RequestMapping("/token")
public class TokenController {

    @Autowired
    private TokenService tokenService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/get")
    public ResponseEntity<?> getToken() throws NoSuchAlgorithmException, JOSEException {
        logger.info("GET request -> getToken()");

        String token = tokenService.createJwtDecryptedToken();

        return ResponseEntity.ok().body(token);
    }

    @GetMapping("/decrypt/{token}")
    public ResponseEntity<?> decryptToken(@PathVariable String token) throws JOSEException, ParseException {
        logger.info("GET request -> decryptToken()");

        String decryptedToken = tokenService.decryptToken(token);

        return ResponseEntity.ok().body(decryptedToken);
    }

    @GetMapping("/keycloak")
    public ResponseEntity<?> getFomKeycloak() throws IOException, ParseException {
        logger.info("GET request -> decryptToken()");

        JSONObject json = tokenService.getFromKeycloak();

        return ResponseEntity.ok().body(json.toJSONString());
    }

    @GetMapping("/jwkset")
    public ResponseEntity<?> getJwkSet() {
        logger.info("GET request -> decryptToken()");

        JSONObject json = tokenService.createJwkSet();

        return ResponseEntity.ok().body(json.toJSONString());
    }


}

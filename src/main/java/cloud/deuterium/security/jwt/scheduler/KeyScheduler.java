package cloud.deuterium.security.jwt.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.security.NoSuchAlgorithmException;

/**
 * Created by MilanNuke on 10/14/2019
 */

@EnableScheduling
@Configuration
public class KeyScheduler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Scheduled(cron = "0 0 0 * * ?") // triggered everyday at 00:00

    public void refreshKeys() throws NoSuchAlgorithmException {
        logger.info("Refreshing private and public keys used for signing jwt...");
        logger.info("Refreshing private and public keys successfully finished. Public key id");
        logger.info("Refreshing public keys from database...");
        logger.info("Refreshing public keys from database successfully finished");
    }

//    @Scheduled(cron = "0 0 */6 ? * *") // triggered every 6 hours
//    @Scheduled(cron = "* * * ? * *") // triggered every second
    @Scheduled(cron = "0 * * ? * *") // triggered every minute
    public void refreshAuthToken() {
        logger.info("Refreshing authentication token...");



        logger.info("Authentication token refreshed successfully");

    }

//    @Scheduled(fixedRate = 1000) // triggered every 6 hours
//    public void test() {
//        logger.info("TEST");
//
//    }

}
